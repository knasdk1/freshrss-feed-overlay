<?php
ini_set('display_errors', 1);
// ================================================================================================
// BOOTSTRAP FreshRSS
require(__DIR__ . '/../../constants.php');
require(LIB_PATH . '/lib_rss.php');    //Includes class autoloader
Minz_Configuration::register('system', DATA_PATH . '/config.php', FRESHRSS_PATH . '/config.default.php');
Minz_Session::init('FreshRSS');
// ================================================================================================

class FeedOverlay extends Minz_ModelPdo {
    protected $entryDAO;
    /** @var false|la */
    private $currentUser;

    public function __construct() {
        parent::__construct();
        $this->currentUser = Minz_Session::param('currentUser', false);
    }

    public function urlExists($url)
    {

        $sql = "SELECT link FROM " . $this->currentUser . "_entry WHERE link LIKE :url LIMIT 1";

        $stm = $this->pdo->prepare($sql);
        $stm->bindValue(':url', $url . '%', PDO::PARAM_STR);
        $stm->execute();

        return count($stm->fetchAll());
    }
}

if (!Minz_Session::param('loginOk', false)) {
    die('Unknown user');
}

if (!$url = $_GET['url']) {
    die('No url provided');
}

$feedOverlay = new FeedOverlay();
$parsedUrl = parse_url($url);
$referer = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];

if (!$feedOverlay->urlExists($referer)){
    die('Unknown url provided');
}

$header = [];
$header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
$header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
$header[] = "Accept-Language: pl,en-us;q=0.7,en;q=0.3";
$header[] = "Cache-Control: max-age=0";
$header[] = "Connection: keep-alive";
$header[] = "Keep-Alive: 300";
$header[] = "Pragma: ";
$header[] = "Origin: " . $referer;
$header[] = "X-Frame-Options: " . $referer;
$header[] = "Host: " . $parsedUrl['host'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_REFERER, $referer);
curl_setopt($ch, CURLOPT_COOKIEJAR, "cookie.txt");
curl_setopt($ch, CURLOPT_COOKIEFILE, "cookie.txt");
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10);
curl_setopt($ch, CURLOPT_COOKIESESSION,true);
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7 (.NET CLR 3.5.30729)');
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_URL, $url);

$html = curl_exec($ch);
$html = preg_replace("/href=(\"|')\/(?!\/)/", 'href=$1' . $referer . '/', $html);
$html = preg_replace("/href=(\"|')..\//", 'href=$1' . $referer . '/../', $html);
$html = preg_replace("/data-image=(\"|')\//", 'data-image=$1' . $referer . '/', $html);
$html = preg_replace("/srcset=(\"|')\//", 'srcset=$1' . $referer . '/', $html);

$html = preg_replace("/src=(\"|')\//", 'src=$1' . $referer . '/', $html);
echo $html;
