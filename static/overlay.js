document.addEventListener('click', function (e) {
    let element = e.target;
    if (element && element.classList) {
        if (element.classList[element.classList.length - 1] === 'feedOverlayOpenButton') {
            feedOverlay.activateIframe(element);
        }

        if (element.classList[element.classList.length - 1] === 'feedOverlayCloseButton') {
            feedOverlay.hideOverlay(element);
        }
    }
});

class feedOverlay {
    topPadding = 85;

    posTop() {
        return (this.topPadding + window.pageYOffset) + 'px';
    }

    windowDimensions() {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight || e.clientHeight || g.clientHeight;

        return {'x': (x-400) + 'px', 'y': (y - this.topPadding - 50) + 'px'}
    }

    activateIframe(element) {
        let windowDimension = this.windowDimensions();
        let webpageContainer = element.nextSibling;
        let overlayWidth = this.getParents(element, '.flux_content')[0].offsetWidth + 'px';
        let fluxContentHeight = this.getParents(element, '.flux_content')[0].clientHeight;

        if (webpageContainer.childNodes.length === 2) {
            webpageContainer.style.width = overlayWidth;
            webpageContainer.style.height = windowDimension.y;
            webpageContainer.style.top = this.posTop();
            webpageContainer.style.display = 'block';

            let frame = webpageContainer.childNodes[1];
            frame.style.width = overlayWidth;
            frame.style.height = windowDimension.y;

            document.body.style.overflow = 'hidden';

            return;
        }

        webpageContainer.style.width = overlayWidth;
        webpageContainer.style.height = windowDimension.y;
        webpageContainer.style.top = this.posTop();

        let frame = document.createElement('IFRAME');
        frame.style.width = overlayWidth;
        frame.style.height = windowDimension.y;
        frame.setAttribute('sandbox', 'allow-same-origin allow-scripts allow-popups allow-forms');
        frame.src = '/i/urlLoader.php/?url=' + encodeURI(webpageContainer.getAttribute('data-url'));

        webpageContainer.appendChild(frame);
        webpageContainer.style.display = 'block';

        frame.addEventListener('mouseover', function(){
            document.body.style.overflow = 'hidden';
            webpageContainer.style.height = windowDimension.y
            frame.style.height = windowDimension.y;
        });
        frame.addEventListener('mouseout', function(){
            document.body.style.overflow = 'scroll';
            webpageContainer.style.height = (fluxContentHeight - 24) + 'px';
            frame.style.height = (fluxContentHeight - 24) + 'px';
        });

        document.body.style.overflow = 'hidden';
        window.addEventListener('keydown', this.escapeKeyEvent.bind(null, element, this));
    }

    escapeKeyEvent(element, feedOverlay, event) {
        if (event.key === 'Escape') {
            feedOverlay.hideOverlay(element.nextSibling.firstChild);
        }
    }

    hideOverlay(element) {
        element.parentNode.style.display = 'none';
        document.body.style.overflow = 'scroll';
        window.removeEventListener('keydown', this.escapeKeyEvent);
    }

    getParents(element, parentSearch) {
        let parents = [];
        let searchType = parentSearch.charAt(0);
        for ( ; element && element !== document; element = element.parentNode ) {
            switch (searchType) {
                case '.':
                    if (element.classList.contains(parentSearch.substr(1))) {
                        parents.push(element);
                    }
                    break;
                case '#':
                    if (element.attr('id') == parentSearch.substr(1)) {
                        parents.push(element);
                    }
                    break;
                default:
                    if (element.tagName.toLowerCase() == parentSearch.toLowerCase()){
                        parents.push(element);
                    }
            }
        }

        return parents;
    };
}

feedOverlay = new feedOverlay();
