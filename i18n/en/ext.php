<?php

return array(
	'feed_overlay' => array(
		'mouseover_expand' => 'Expand overlay on mouse over',
        'updates' => 'Get the updated version',
        'here' => 'here',
	),
);
