# Install info
FreshRSS needs the CURL URL loader file (**p/i/urlLoader.php**), which must be copied to FreshRSS root folder, corresponding to the path for the file - e.g. **/p/i/**
From the extension dir /extensions/feed-overlay run `cp p/i/urlLoader.php ../p/i/`