<?php

class FeedOverlayExtension extends Minz_Extension
{
    /** @var bool */
    protected $mouseover_expand = true;

    public function init()
    {
        $extension = Minz_ExtensionManager::findExtension('Feed Overlay');
        Minz_View::appendStyle($extension->getFileUrl('overlay.css', 'css'));
        Minz_View::appendScript($extension->getFileUrl('overlay.js', 'js'));
        $this->registerHook('entry_before_display', array($this, 'embedFeedInOverlay'));
        $this->registerTranslates();
    }

   public function loadConfigValues()
   {
       if (!class_exists('FreshRSS_Context', false) || null === FreshRSS_Context::$user_conf) {
           return;
       }

       if (FreshRSS_Context::$user_conf->mouseover_expand_overlay != '') {
           $this->mouseover_expand = FreshRSS_Context::$user_conf->mouseover_expand;
       }
    
   }

    public function getMouseoverExpand(): bool
    {
        return $this->mouseover_expand;
    }

    public function embedFeedInOverlay(FreshRSS_Entry $entry): FreshRSS_Entry
    {
        $link = $entry->link();

        $entryContent = '<button class="btn btn-attention feedOverlayOpenButton">View article</button><div class="feedOverlay" data-url="' . $link . '"><div class="feedOverlayCloseButton">X</div></div>' .
        $entry->Content();
        $entry->_content($entryContent);

        return $entry;
    }

   /**
    * Saves the user settings for this extension.
    */
   public function handleConfigureAction()
   {
       $this->loadConfigValues();
       if (Minz_Request::isPost()) {
           FreshRSS_Context::$user_conf->mouseover_expand = (int)Minz_Request::param('mouseover_expand', '');
           FreshRSS_Context::$user_conf->save();
       }
   }
}
